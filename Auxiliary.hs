module Auxiliary where

import Config
import Types

between :: [a] -> [a] -> [a] -> [a]
between lft rht mid = lft ++ mid ++ rht

shiftLeft :: [a] -> [a]
shiftLeft xs = (tail xs) ++ [head xs]

backslash :: String
backslash = "\\"

lineend :: Command
lineend = "\\\\"

onlyJust :: [Maybe a] -> [Maybe a]
onlyJust = filter (\ x -> case x of Nothing -> False
                                    Just _  -> True)
