module Config where

defaultIndent :: Int
defaultIndent = 2

-- Defines maximal line length in .tex file
defaultLineLength :: Int
defaultLineLength = 50
