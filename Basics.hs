module Basics where

import Auxiliary
import Config
import Text
import Types

import Data.List

placeText :: Text -> Text
placeText = setDefaultLength

call :: CommandName -> Command
call = (backslash++)

fillBrackets :: Bracket -> Bracket -> Delim -> [String] -> Block
fillBrackets _ _ _ [] = []
fillBrackets lft rht delim contents =
  block lft rht $ intercalate delim contents

optionsBlock :: [Option] -> Block
optionsBlock = fillBrackets "[" "]" ", "

paramsBlock :: [Param] -> Block
paramsBlock = fillBrackets "{" "}" ", "

-- Command calling

-- \command{args}[options]{args}

command :: CommandName -> [Param] -> [Option] -> [Param] -> Command
command cmd lftPars opts rhtPars =
  concat [ call cmd
         , paramsBlock lftPars
         , optionsBlock opts
         , paramsBlock rhtPars ]

-- Preambule

documentclass :: [Option] -> DocumentClass -> Command
documentclass opts docType = command "documentclass" [] opts [docType]

usepackage :: [Option] -> Package -> Command
usepackage opts pack = command "usepackage" [] opts [pack]

preambule
  :: ([Option], DocumentClass)
  -> [([Option], Package)]
  -> [Command]
  -> Command
preambule docPars pacPars cmds = intercalate "\n" genPreambule
  where
    genPreambule = docClass ++ packages ++ cmds
    docClass     = [uncurry documentclass docPars]
    packages     = map (uncurry usepackage) pacPars  

-- Begin \ End

begin :: BlockType -> [Option] -> [Param] -> Command
begin blType = command "begin" [blType]  

end :: BlockType -> Command
end blType = command "end" [blType] [] []

begend :: BlockType -> [Option] -> [Param] -> Contents -> Block
begend blType opts pars =
  indentedBlock (begin blType opts pars) (end blType)
  
-- Tabular

tabular :: [Option] -> [Param] -> TableBody -> Command
tabular = begend "tabular"

hline :: Command
hline = call "hline"

-- Auto generates square table by given list of contents

squareTabular :: [Option] -> [Param] -> [[Text]] -> Command
squareTabular opts pars contents = tabular opts pars genBody
  where
    separate l = intercalate " " [l, lineend, hline]
    genBody =
      intercalate "\n" $
        between [hline] [hline] $
          map (separate . intercalate " & ") contents
