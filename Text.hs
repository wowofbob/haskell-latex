module Text where

import Auxiliary
import Config
import Types

import Data.List

newline :: Text -> Text
newline = ('\n':)

rndBrks :: Text -> Text
rndBrks = between "(" ")"

crvBrks :: Text -> Text
crvBrks = between "{" "}"

-- Indentation

indent :: Int -> Text -> Text
-- init for removing last '\n' symbol
indent n = init . unlines . map (space ++) . lines
  where
    space = replicate n ' '
    
indentByDefault :: Text -> Text
indentByDefault = indent defaultIndent

block :: Delim -> Delim -> Text -> Block
block = between

indentedBlock :: Delim -> Delim -> Text -> Block
indentedBlock upperBoard lowerBoard contents =
  intercalate "\n" [upperBoard, indentByDefault contents, lowerBoard]
  
-- Line splitting

setLineLength :: Int -> Text -> Text
setLineLength len txt = splitOnLines txt []
  where
    splitOnLines [] splitted = intercalate "\n" splitted
    splitOnLines txt splitted = let
      (line, rest) = splitAt len txt
      in splitOnLines rest (splitted ++ [line])

setDefaultLength :: Text -> Text
setDefaultLength = setLineLength defaultLineLength
