module Types where

type Bracket = String
type Command = String
type Delim   = String
type Name    = String
type Option  = String
type Package = String
type Param   = String
type Pos     = String
type Text    = String

type Contents = String

type CommandName   = String
type DocumentClass = String

type Block     = String
type BlockType = String

type TableBody = String
type TikzBody  = String
