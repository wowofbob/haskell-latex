module Tikz where

-- Summary: this module allows to draw a simple cycles and trees.

-- TODO: Hide auxiliary functions

import Auxiliary
import Basics
import Text
import Types

import Data.List

type Content  = String
type NodeName = String
type Object   = String
type Position = String

-- Tikz Node representation

data Node = Node [Option] NodeName Position Content deriving Show

nodeName :: Node -> NodeName
nodeName (Node _ n _ _) = n 

type Cycle = [Node]

data Tree = TEmpty | TNode Node Tree Tree deriving Show

-- For testing purposes:

testTree =
  TNode (Node [] "1" "1:1" "1")
    (TNode (Node [] "2" "2:2" "2") TEmpty TEmpty)
    (TNode (Node [] "3" "3:3" "3")
      (TNode (Node [] "4" "4:4" "4") TEmpty TEmpty)
      (TEmpty))

getNode :: Tree -> Maybe Node
getNode TEmpty        = Nothing
getNode (TNode n _ _) = Just n

type Edge = (Node, Node)

getEdges :: Tree -> [Edge]
getEdges = map (\ (Just x) -> x) . onlyJust . extract
  where
  
    extract TEmpty = []
    extract (TNode n lft rht) =
      let
        lftEdge = mkEdge n lft
        rhtEdge = mkEdge n rht
      in lftEdge : rhtEdge : ((extract lft) ++ (extract rht))  
    
    mkEdge n t =
      case getNode t of Nothing -> Nothing
                        Just m  -> Just (n, m)

type Style = (Object, [Param])

-- \tikzstyle

style :: Object -> [Param] -> Command
style obj pars = concat $
  [call "tikzstyle", paramsBlock [obj], "=", optionsBlock pars]


-- Drawing Node

node :: [Option] -> NodeName -> Position -> Content -> Command
node opts name pos content =
  intercalate " " $
    [ command "node" [] opts []
    , between "(" ")" name
    , "at"
    , between "(" ")" pos
    , between "{" "}" content
    , ";" ]

drawNode :: Node -> Command
drawNode (Node opts name pos content) = node opts name pos content

-- tikzpickture Figure

tikzpicture :: [Option] -> Content -> Block
tikzpicture opts = begend "tikzpicture" opts []

-- \from, \to

from :: Command
from = call "from"

to :: Command
to = call "to"

-- \draw

draw :: [Option] -> NodeName -> NodeName -> Command
draw opts lft rht = intercalate " "
  [call "draw", optionsBlock opts, connectNodes, ";"]
  where
    connectNodes = intercalate " "
      [ between "(" ")" lft
      , "--"
      , between "(" ")" rht ]

-- \foreach

foreach :: [Option] -> [NodeName] -> Command
foreach opts nodes = intercalate " "
  [ call "foreach"
  , "\\from/\\to"
  , "in"
  , paramsBlock connectNodes
  , newline $
      indentByDefault $
        draw opts from to ]  
  where
    connectNodes = zipWith (\ x y -> x ++ "/" ++ y) nodes (shiftLeft nodes)

-- Drawing Cycle

drawCycle :: [Option] -> Cycle -> Block
drawCycle opts nodes = tikzpicture opts mkContent  
  where
    mkStyle   = style "every node" ["shape=circle"]
    mkNodes   = map drawNode nodes
    mkLines   = foreach ["->"] $ map nodeName nodes
    mkContent =
      intercalate "\n" $ 
        ([mkStyle] ++ mkNodes ++ [mkLines])

-- Drawing Tree

drawTree :: [Option] -> Tree -> Block
drawTree opts tree = tikzpicture opts mkContent
  where
    
    mkContent =
      intercalate "\n" $
        mkStyle : (concatMap drawEdge edges) 

    mkStyle = style "every node" ["shape=circle"]    
    
    edges   = getEdges tree
    
    drawEdge (n, m) = let
      nName = nodeName n
      mName = nodeName m
      in [ drawNode n
         , drawNode m
         , draw ["->"] nName mName ]
    
    
